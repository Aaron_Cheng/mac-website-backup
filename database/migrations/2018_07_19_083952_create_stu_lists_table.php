<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStuListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stu_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('acct')->nullable();
            // $table->string('name')->nullable();
            $table->string('is_pass')->default('否');
            $table->integer('appuser_id')->nullable();
            $table->integer('class_id')->nullable();
            $table->integer('type')->nullable(); //1=>個別課程 2=>套裝課程
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stu_lists');
    }
}
