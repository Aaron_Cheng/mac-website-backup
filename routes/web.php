<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//test
Route::get('/entry', function () {
    return view('entry');
});
//根目錄
Route::get('/', 'EntryController@index');

//首頁
Route::get('/Homepage','AppUserController@refreshhomepage');

//公告頁面
Route::get('/News','NewsController@refresh_news');
Route::get('/News/counter/{id}','NewsController@updateCounter');

//管理員登入
Route::get('/authrize', function () {
    return view('authrize.login');
});

Route::get('authrize/menu', function () {
    return view('authrize.menu');
})->middleware('auth');

//新增管理者
Route::get('authrize/menu/addsuperuser', function () {
    return view('authrize.addsuperuser');
})->middleware('auth');

Route::post('/authrize/menu/addsuperuser','superuserController@create')->middleware('auth');

// 檢索管理員
Route::get('/authrize/menu/superuserList','superuserController@index')->middleware('auth');

// 刪除管理員
Route::resource('/authrize/menu/superuserList','superuserController');

//最新公告管理
Route::resource('authrize/menu/news','NewsController');


//最新公告圖片管理
Route::resource('authrize/menu/photo','PhotoController');

//管理所有課程功能
//single
Route::get('authrize/menu/classcontrol','AppUserController@showclass');

Route::post('/application/edit_single_in_auth/{id}','Single_classController@edit_fin_auth');
Route::get('/application/deleteSingle_auth/{id}','Single_classController@delete_auth');

Route::get('/application/view_module/{id}','Module_classController@view');
//edit module class
Route::post('/application/edit_module_auth/{id}','Module_classController@edit_fin_auth');
//delete module class
Route::get('/application/deleteModule_auth/{id}','Module_classController@delete_auth');




Route::get('/authrize/menu/addClient','AppUserController@addApplicant')->middleware('auth');


Route::get('authrize/menu/contralAchievement','AchievementController@indexshow')->middleware('auth');

Route::delete('authrize/achievement/{id}','AchievementController@achievement_delete')->middleware('auth');

//查看各單位學生修課狀況
Route::get('authrize/menu/classmanage','ClassManageController@authindex')->middleware('auth');
Route::get('authrize/menu/classmanage/{id}','ClassManageController@authshow')->middleware('auth');
Route::post('authrize/menu/classmanage/search/{id}','ClassManageController@search')->middleware('auth');
Route::get('/authrize/classmanage/{type}/{class_id}/{admin_id}','ClassManageController@authmanagement')->middleware('auth');
Route::get('/authrize/classmanage/export/excel/{class_id}/{admin_id}','ClassManageController@export')->middleware('auth');
Route::get('/authrize/export/all/{term}','ClassManageController@export_all')->middleware('auth');

Route::get('authrize/menu/uploadAchievement', function () {
    return view('authrize.uploadachievement');
});


Route::get('/authrize/achievement','AppUserController@storeachievement');
Route::post('/authrize/achievement','AppUserController@storeachievement');

//寄信用
Route::post('/authrize/menu/addClient/sendmail','AppUserController@sentMailTo');


// Route::post('/authrize/sentMailTo','AppUserController@sentMailTo');

Route::get('authrize', function () {
    return view('authrize.login');
});

//編輯開課單位資訊功能
Route::get('/authrize/editClient/{id}','AppUserController@editClient');
Route::post('/authrize/editClient/{id}','AppUserController@edit_fin');


Route::get('/authrize/deleteapplicants/{id}','AppUserController@delete');

Route::get('authrize', function () {
    return view('authrize.login');
});

//管理員編輯簡介
Route::get('/authrize/menu/introduce', function () {
    return view('authrize/introduce/index');
});
Route::get('/authrize/menu/introduce/QuestionEdit', function () {
    return view('authrize.introduce.edit');
});
Route::get('/authrize/menu/introduce/TypeEdit', function () {
    return view('authrize.introduce.TypeEdit');
});
Route::get('/authrize/menu/introduce/StepEdit', function () {
    return view('authrize.introduce.StepEdit');
});
Route::get('/authrize/menu/introduce/ManualEdit', function () {
    return view('authrize.introduce.ManualEdit');
});
Route::get('/authrize/menu/introduce/ContactEdit', function () {
    return view('authrize.introduce.ContactEdit');
});
Route::get('/authrize/menu/introduce/RuleEdit', function () {
    return view('authrize.introduce.RuleEdit');
});

Route::post('/authrize/menu/introduce/RuleEdit', 'IntroduceController@Rulestore');
Route::delete('/authrize/menu/introduce/{introduce_classtypes}', 'IntroduceController@RuleDestroy');

Route::get('/authrize/menu/introduce', 'IntroduceController@show');
Route::get('/authrize/menu/introduce/QuestionEdit', 'IntroduceEditController@IntroduceQuestion');
Route::get('/authrize/menu/introduce/TypeEdit', 'IntroduceController@Typeshow');
Route::get('/authrize/menu/introduce/StepEdit', 'IntroduceController@Stepshow');
Route::get('/authrize/menu/introduce/ManualEdit', 'IntroduceController@Manualshow');
Route::get('/authrize/menu/introduce/ContactEdit', 'IntroduceController@Contactshow');

Route::post('/authrize/menu/introduce/TypeEdit/{introduce_classtypes}', 'IntroduceController@Typeupdate');
Route::post('/authrize/menu/introduce/StepEdit/{introduce_classsteps}', 'IntroduceController@Stepupdate');
Route::post('/authrize/menu/introduce/ManualEdit/{introduce_manuals}', 'IntroduceController@Manualupdate');
Route::post('/authrize/menu/introduce/ContactEdit/{introduce_contacts}', 'IntroduceController@Contactupdate');
Route::post('/introduce/edit/{introduce_question}', 'IntroduceEditController@update');
Route::delete('/introduce/edit/{introduce_question}', 'IntroduceEditController@destroy');
//簡介
Route::get('/introduce', 'IntroduceController@IntroduceQuestion');
Route::post('/introduce/edit', 'IntroduceEditController@Questionstore');

// //審核單位入口
// Route::get('/check',function() {
//   return view('check.index');
// });
// Route::get('/check/ckeckclass',function() {
//   return view('check.checkclass');
// });
// Route::get('/check/checkstudents',function() {
//   return view('check.checkstudents');
// });
// Route::get('/check/checkprojects',function() {
//   return view('check.checkprojects');
// });
// Route::get('/check/register',function() {
//   return view('check.register');
// });


//成果展示
Route::get('/achievement', 'AchievementController@index');
Route::get('/achievement/past', 'AchievementController@index_past');
Route::get('/achievement/{id}', 'AchievementController@index_item');
Route::get('/achievement/achievement', 'AchievementController@index');
//成果編輯
Route::get('/achievement/edit/{id}', 'AchievementController@edit');
Route::post('/achievement/edit/{id}', 'AchievementController@edit_fin');
//成果查詢
// Route::get('achievement/search', 'AchievementController@search');
Route::post('achievement/search', 'AchievementController@search');

//開課單位入口
Route::get('/openclass',function() {
  return view('openclass.index');
});
Route::get('/openclass/applied',function(){
  return view('openclass.applied');
});
Route::get('/openclass/apply',function(){
  return view('openclass.apply');
});
Route::get('/openclass/classmanage',function(){
  return view('openclass.classmanage');
});
Route::get('/openclass/classmanage/class',function(){
  return view('openclass.class');
});

//application
//log in
Route::get('/application', 'AppUserController@index');
Route::get('/application/logout', 'AppUserController@logout');
Route::post('/application', 'AppUserController@login');
Route::post('/application/addappUser', 'AppUserController@register'
);
//edit password
Route::get('/application/edit_pwd', 'AppUserController@edit_goto');
Route::post('/application/edit_pwd', 'AppUserController@edit_pwd');
//manage
Route::get('/application/mgt', function () {
    return view('application.mgt');
});
//browse class
Route::get('/application/class', function () {
    return view('application.class');
});
Route::get('/application/choose', function () {
    return view('application.choose');
});

Route::get('/application/uploadAchievement', function () {
    return view('application.uploadachievement_byclass');
});
//新增成果上傳
Route::get('/application/contralAchievement','AchievementController@application_indexshow');

//new single class
Route::get('/application/single','Single_classController@index');
Route::post('/application/single','Single_classController@store');
//view single class
Route::get('/application/view_single/{id}','Single_classController@view');
//edit single class
Route::get('/application/edit_single/{id}','Single_classController@edit');
Route::post('/application/edit_single/{id}','Single_classController@edit_fin');

//delete single class
Route::get('/application/deleteSingle/{id}','Single_classController@delete');

//new module class
Route::get('/application/module','Module_classController@index');
Route::post('/application/module','Module_classController@store');
Route::post('/application/module/addfile/{id}','Module_classController@addfile');
Route::get('/application/module/delfile/{id}','Module_classController@delfile');
Route::get('/application/module/getfile/{id}','Module_classController@getfile');
//view module class
Route::get('/application/view_module/{id}','Module_classController@view');
//edit module class
Route::get('/application/edit_module/{id}','Module_classController@edit');
Route::post('/application/edit_module/{id}','Module_classController@edit_fin');
//delete module class
Route::get('/application/deleteModule/{id}','Module_classController@delete');

//module
//add module
Route::post('/application/add_module','ModuleController@store');
//edit small module
Route::post('/application/edit_smallmodule/{id}','ModuleController@edit_fin');
//delete module
Route::get('/application/deleteSmallModule/{id}','ModuleController@delete');

//new fractal class
Route::get('/application/fractal','Fractal_classController@index');
Route::post('/application/fractal','Fractal_classController@store');
//view fractal
Route::get('/application/view_fractal/{id}','Fractal_classController@view');
//edit fractal
Route::get('/application/edit_fractal/{id}','Fractal_classController@edit');
Route::post('/application/edit_fractal/{id}','Fractal_classController@edit_fin');
//delete fractal
Route::get('/application/deleteFractal/{id}','Fractal_classController@delete');

//finish
Route::get('/application/finish', function () {
    return view('application.finish');
});
//開課單位新增成果
Route::post('/application/achievement','AppUserController@storeachievement_byClass');

//管理班級名單/是否通過
Route::get('/application/classmanage/{type}/{class_id}/{admin_id}','ClassManageController@index');
Route::post('/excel/import/{class_id}/{admin_id}','ClassManageController@import');
Route::get('/application/classmanage/delete_user/{class_id}','ClassManageController@destroy');

//歷年紀錄
Route::get('record','SearchController@indexall');

Route::get('record/view_module/{id}','ViewClassController@view_module');

Route::get('record/當學期微課程','SearchController@all2');

Route::get('search', 'SearchController@index');
Route::post('search2', 'SearchController@search2');

//課程影音
Route::get('/video', 'VideoController@index');
Route::get('/video/video','VideoController@index');



Auth::routes();
