<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntroduceManual extends Model
{
    protected $fillable = ['body'];
}
