<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stu_list extends Model
{
    protected $fillable=['acct','name','is_pass'];
}
