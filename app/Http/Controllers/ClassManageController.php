<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\appUser;
use App\Module_class;
use App\Single_class;
use App\Fractal_class;
use App\Stu_list;
use DB;
use Excel;
use Storage;
use Carbon;
use Chumper\Zipper\Zipper;
use Illuminate\Filesystem\Filesystem;
class ClassManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type,$class_id, $admin_id)
    {
        $acct = appUser::find($admin_id)->account;
        $class= Module_class::where(['account'=>$acct, 'id'=>$class_id])->first();
        $stu = Stu_list::where(['class_id'=>$class_id])->get();
        return view('application/classmanage.index',compact('class','class_id','admin_id','stu'));

    }

    public function import($class_id, $admin_id,Request $request){
      $name=pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME);
      $ext=pathinfo($request->file->getClientOriginalName(), PATHINFO_EXTENSION);
      if($ext == 'xlsx' || $ext =='xls'){
        $fname=time().$name.'.'.$ext;
        $request->file->move(public_path('exports'), $fname);
        $filePath = 'public/exports/'.$fname;
        $data = Excel::load($filePath, function($reader) {} )->all();
          // dd($data);
        $num = count($data);
         for ($i=0; $i < $num; $i++) {
           $stu = new Stu_list;
           $stu->acct = $data[$i]->s_id; //!!注意 excel中的header名稱必須跟這邊的名稱相同為"student_id"
           $stu->is_pass = $data[$i]->is_pass; //!!注意 excel中的header名稱必須跟這邊的名稱相同為"name"
           $stu->class_id = $class_id;
           $stu->appuser_id = $admin_id;
           $stu->save();
         }
        unlink(public_path('exports').'/'.$fname);
        return back()->with('success','success')->with('name',$name.'.'.$ext);
      }else{
        return back()->with('alert', '上傳格式須為xlsx或xls，感謝您的配合!');
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dest = Stu_list::where(['class_id'=>$id]);
        $dest->delete();
        return back()->with('d_info','d_info');
    }
    //以下為管理者後台(ex:碧華姐的角色)檢視各個開課單位的修課單位的function部分//
    public function authindex(){
      $applicants = appUser::all();
      $year = date("Y")-1911;
      $year2 = $year-1;
      $mon = date("m");
      if($mon>7||$mon<2){
        $d_one = $year.'-1';
        $d_two = $year2.'-2';
      }else{
        $d_one = $year2.'-2';
        $d_two = $year2.'-1';
      }
      return view('authrize/classmanage.index',[
        "applicants"=>$applicants,
        "d_one"=>$d_one,
        "d_two"=>$d_two,
      ]);
    }

    public function authshow($id){
      $year = date('Y')-1911;
      $zone = array($year,$year-1,$year-2);
      $user_account = $id;

      $module_classes = Module_class::where('account',$user_account)
        ->Where(function ($query) use($zone) {
             for ($i = 0; $i < count($zone); $i++){
                $query->orwhere('term', 'like',  $zone[$i] .'%');
             }
        })->orderBy('term','desc')->get();
        //dd($module_classes);
      // $fractal_classes = Fractal_class::where('account',$user_account)->get();
      $admin = appUser::where('account',$user_account)->get()->take(1);
      $admin_id = $admin[0]->id;

      return view('authrize/classmanage.show',[
        "module_classes"=>$module_classes,
        "admin_id"=>$admin_id,
        "user_account"=>$user_account,
      ]);
    }
    public function authmanagement($type,$class_id, $admin_id)
    {
        $acct = appUser::find($admin_id)->account;
        $class= Module_class::where(['account'=>$acct, 'id'=>$class_id])->first();
        $stu = Stu_list::where('class_id',$class_id)->get();
        return view('authrize/classmanage.mgt',compact('class','class_id','admin_id','stu'));


    }
    public function search($id,Request $request){
      if($request->name == null){

        $module_classes = Module_class::where('account',$id)
            ->Where('term',$request->term)->orderBy('term','desc')->get();
      }else {

        $module_classes = Module_class::where('account',$id)
            ->Where('term',$request->term)
            ->Where('name','like','%'.$request->name.'%')
            ->orderBy('term','desc')
            ->get();
      }
      $nothing = 0;//判斷是否有資料
      if($module_classes->count() == 0){
        $nothing = 1;
      }
      $admin = appUser::where('account',$id)->get()->take(1);
      $admin_id = $admin[0]->id;
      return view('authrize/classmanage.search',compact('module_classes','admin_id','nothing'));

    }

    public function export_all($term){
      $term_excel = str_replace('-','',$term);
      $class = Module_class::where('term',$term)->get();
      foreach ($class as $key => $value) {
        $stu=Stu_list::where('class_id',$value->id)->get();
        if($value->obligatory=="選修"){
          $obligatory = 2;
        }else {
          $obligatory = 1;
        }
        $cellData[] = array(
          // '0' => 'semester', // excel 的header
          '0' => 'semester', // excel 的header
          '1' => 'crs_no',
          '2' => 'crs_type_no',
          '3' => 's_id',
          '4' => 'is_pass',

        );
        foreach ($stu as $stu_key => $stu_value) {
          $cellData[] = array(
            '0' => $term_excel, // excel 的data
            '1' => $value->number,
            '2' => $obligatory ,
            '3' => $stu_value->acct,
            '4' => $stu_value->is_pass,
         );
        }

          $filename = time().$value->name;
          Excel::create($filename,function($excel) use ($cellData){
              $excel->sheet('score', function($sheet) use ($cellData){
                  $sheet->rows($cellData);
              });
          })->store('xls');
          unset($cellData);
      }
      //把之前的資料刪掉(zip)
      $dzfiles = glob(storage_path('zip/*'));
      foreach($dzfiles as $dzfile){
        if(is_file($dzfile))
          unlink($dzfile);
      }
      //check exports裡面是否有file
      $FileSystem = new Filesystem();
      if ($FileSystem->exists(storage_path('exports'))) {
        $exports_files = $FileSystem->files(storage_path('exports'));
        if (empty($exports_files)) {
         return back()->with('no_file','沒有檔案!');
        }
      }
      //確定有file後再加成zip檔下載
      $zipper=new Zipper();
      $files = glob(storage_path('exports'));   //storage_path被壓縮文件名
      $zipper->make(storage_path('zip/'.$term_excel.'.zip'))->add($files)->close();  //storage_path($reduce_path)壓縮後要叫甚麼
      $dfiles = glob(storage_path('exports/*')); // get all file names
      foreach($dfiles as $dfile){ // iterate files
        if(is_file($dfile))
          unlink($dfile); // delete file Q
      }
      return response()->download(storage_path('zip/'.$term_excel.'.zip'));
    }


    public function export($class_id, $admin_id){
        $acct = appUser::find($admin_id)->account;
        $class= Module_class::where(['account'=>$acct, 'id'=>$class_id])->first();
        $stu = Stu_list::where(['class_id'=>$class_id])->get();
        $term = str_replace('-','',$class->term);
        if($class->obligatory=="選修"){
          $obligatory = 2;
        }else {
          $obligatory = 1;
        }
        $cellData = array();
        $cellData[] = array(
          // '0' => 'semester', // excel 的header
          '0' => 'semester', // excel 的header
          '1' => 'crs_no',
          '2' => 'crs_type_no',
          '3' => 's_id',
          '4' => 'is_pass',

        );
        foreach ($stu as $key => $value) {
          $cellData[] = array(
            '0' => $term, // excel 的data
            '1' => $class->number,
            '2' => $obligatory ,
            '3' => $value->acct,
            '4' => $value->is_pass,
         );
        }

          // $filename = $class->term.$class->name;
          $filename = time().$class->name;
          Excel::create($filename,function($excel) use ($cellData){
              $excel->sheet('score', function($sheet) use ($cellData){
                  $sheet->rows($cellData);
              });
          })->export('xls');



    }
}
