<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module_class;
use App\Single_class;
use App\Fractal_class;
use DB;

class SearchController extends Controller
{
    public function index(){
      return view('record.search');
    }
    public function indexall(){
      $single_class=Single_class::all();
      $module_class=Module_class::all();
      $fractal_class=Fractal_class::all();
      return view('record.index');
    }

    public function search2(Request $request){
      $name=$request->name;
      $tag=$request->tag;
        if($tag==null){
          if($name==null){
            $search_other=Module_class::where('term',$request->term)->get();
          }else{
            $search_other=Module_class::where('term',$request->term)
                                        ->where('name','like','%'.$name.'%')
                                        ->get();
          }

        }else{
          if($name==null){
            $search_other=Module_class::where('term',$request->term)
                                        ->where('keyword','like','%'.$tag.'%')
                                        ->get();
          }else{
            $search_other=Module_class::where('term',$request->term)
                                        ->where('name','like','%'.$name.'%')
                                        ->where('keyword','like','%'.$tag.'%')
                                        ->get();
          }

        }

        return back()
          ->with('search_other',$search_other);

    }


    public function all2(){
      $year = date("Y")-1911;
      $year2 = $year-1;
      $mon = date("m");
      if($mon>7){ //8~12月算上學期=>ex:2018年9月是107-1學年
        $page=Module_class::where('term','like',$year.'%')->paginate(15);
        $module_class=Module_class::where('term','like',$year.'%')->paginate(15);
      }elseif ($mon<2) { //1月算上學習=>ex:2019年1月是107-1學年
        $page=Module_class::where('term','like',$year2.'%')->paginate(15);
        $module_class=Module_class::where('term','like',$year2.'%')->paginate(15);
      }
      else { //2月~7月算下學期
        $page=Module_class::where('term','like',$year2.'%')->paginate(15);
        $module_class=Module_class::where('term','like',$year2.'%')->paginate(15);
      }


      return view('record.module_class',["module_class"=>$module_class,"page"=>$page]);
    }


    //以下為個別課程的部分但被砍了qq
    public function search(Request $request){
      $name=$request->name;
      $day=$request->weekofday;
      // $time=$request->time;
      $tag=$request->tag;
        if($day==0){
          if($tag==null){
            if($name==null){
              $search_single=Single_class::where('term',$request->term)->get();
            }else{
              $search_single=Single_class::where('term',$request->term)
                                          ->where('name','like','%'.$name.'%')
                                          ->get();

                                        }
          }else{
            if($name==null){
                $search_single=Single_class::where('term',$request->term)
                                              ->where('keyword','like','%'.$tag.'%')
                                              ->get();

            }else{
                $search_single=Single_class::where('term',$request->term)
                                        ->where('name','like','%'.$name.'%')
                                        ->where('keyword','like','%'.$tag.'%')
                                        ->get();
            }
          }
        }else{
          if($name==null){
            if($tag==null){
              $search_single=Single_class::where('term',$request->term)
                                          ->where('weekday','=',$day)
                                          ->get();
            }else{
              $search_single=Single_class::where('term',$request->term)
                                          ->where('weekday','=',$day)
                                          ->where('keyword','like','%'.$tag.'%')
                                          ->get();
            }

          }else{
            if($tag==null){
              $search_single=Single_class::where('term',$request->term)
                                          ->where('name','like','%'.$name.'%')
                                          ->where('weekday','=',$day)
                                          ->get();
            }else{
              $search_single=Single_class::where('term',$request->term)
                                          ->where('name','like','%'.$name.'%')
                                          ->where('weekday','=',$day)
                                          ->where('keyword','like','%'.$tag.'%')
                                          ->get();
            }
          }
        }
        return back()
          ->with('search_single',$search_single);

    }

    public function all(){
      $year = date("Y")-1911;
      $year2 = $year-1;
      $year3 = $year2-1;
      $page=Single_class::where('term','like',$year.'%')->orWhere('term','like',$year2.'%')->orWhere('term','like',$year3.'%')->paginate(15);
      $single_class=Single_class::where('term','like',$year.'%')->orWhere('term','like',$year2.'%')->orWhere('term','like',$year3.'%')->paginate(15);
      return view('record.single_class',["single_class"=>$single_class,"page"=>$page]);
    }




}
