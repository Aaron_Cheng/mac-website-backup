<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\IntroduceQuestion;
Use App\IntroduceClasstype;
Use App\IntroduceClassstep;
Use App\IntroduceManual;
Use App\IntroduceContact;
Use File;
Use App\IntroduceRule;

class IntroduceController extends Controller
{
	// 觀看者的簡介頁
	public function IntroduceQuestion()
	{
		$introduce_questions = IntroduceQuestion::all();
		$introduce_classtypes = IntroduceClasstype::all();
		$introduce_classsteps = IntroduceClassstep::all();
		$introduce_manuals = Introducemanual::all();
		$introduce_contacts = IntroduceContact::all();
		$introduce_rules = IntroduceRule::all();
		return view('introduce.index',['introduce_questions'=>$introduce_questions, 'introduce_classtypes'=>$introduce_classtypes, 
			'introduce_classsteps'=>$introduce_classsteps, 
			'introduce_manuals'=>$introduce_manuals,
			'introduce_contacts'=>$introduce_contacts,
			'introduce_rules'=>$introduce_rules]);
	}

	// 管理者的簡介頁
	public function show()
	{
		$introduce_questions = IntroduceQuestion::all();
		$introduce_classtypes = IntroduceClasstype::all();
		$introduce_classsteps = IntroduceClassstep::all();
		$introduce_manuals = Introducemanual::all();
		$introduce_contacts = IntroduceContact::all();
		$introduce_rules = IntroduceRule::all();
		return view('authrize.introduce.index',['introduce_questions'=>$introduce_questions, 'introduce_classtypes'=>$introduce_classtypes, 
			'introduce_classsteps'=>$introduce_classsteps, 
			'introduce_manuals'=>$introduce_manuals,
			'introduce_contacts'=>$introduce_contacts,
			'introduce_rules'=>$introduce_rules]);
	}

	public function Typeshow()
	{
		$introduce_classtypes = IntroduceClasstype::all();
		return view('authrize.introduce.TypeEdit',['introduce_classtypes'=>$introduce_classtypes]);
	}

	public function Stepshow()
	{
		$introduce_classsteps = IntroduceClassstep::all();
		return view('authrize.introduce.StepEdit',['introduce_classsteps'=>$introduce_classsteps]);
	}
	public function Manualshow()
	{
		$introduce_manuals = Introducemanual::all();
		return view('authrize.introduce.ManualEdit',['introduce_manuals'=>$introduce_manuals]);
	}
	public function Contactshow()
	{
		$introduce_contacts = IntroduceContact::all();
		return view('authrize.introduce.ContactEdit',['introduce_contacts'=>$introduce_contacts]);
	}

	// 法規新增
	public function Rulestore(Request $request){
        $rule=new IntroduceRule;
        $rule->title=$request->title;
        $rule->classification=$request->classification;

        $name=pathinfo($request->rule->getClientOriginalName(), PATHINFO_FILENAME);
        $ext=pathinfo($request->rule->getClientOriginalName(), PATHINFO_EXTENSION);
        $fname=$name.'.'.$ext;
        $request->rule->move(public_path('/document'), $fname);
        $rule->path=$fname;
        $rule->save();
      return redirect('/authrize/menu/introduce');
    }

	// 法規刪除
    public function RuleDestroy($id){
      $rule=IntroduceRule::find($id);
      $title=$rule->title;
      $image_path = public_path().'/document/'.$rule->path;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }
      IntroduceRule::destroy($id);
      return back()->with('success','success')->with('title',$title);
    }

	// 更新
	public function Typeupdate(Request $request, IntroduceClasstype $introduce_classtypes)
	{
		// $introduce_questions->update(['question' => $request->question]);
		$introduce_classtypes->body = $request->editor1;
		$introduce_classtypes->save();
		return redirect('/authrize/menu/introduce/TypeEdit');
	}

	public function Stepupdate(Request $request, IntroduceClassstep $introduce_classsteps)
	{
		$introduce_classsteps->body = $request->editor1;
		$introduce_classsteps->save();
		return redirect('/authrize/menu/introduce/StepEdit');
	}

	public function Manualupdate(Request $request, IntroduceManual $introduce_manuals)
	{
		$introduce_manuals->body = $request->editor1;
		$introduce_manuals->save();
		return redirect('/authrize/menu/introduce/ManualEdit');
	}

	public function Contactupdate(Request $request, IntroduceContact $introduce_contacts)
	{
		$introduce_contacts->body = $request->editor1;
		$introduce_contacts->save();
		return redirect('/authrize/menu/introduce/ContactEdit');
	}

}

