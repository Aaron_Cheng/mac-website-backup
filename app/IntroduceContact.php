<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntroduceContact extends Model
{
    protected $fillable = ['body'];
}
