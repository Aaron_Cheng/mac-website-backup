<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classes_file extends Model
{
    protected $fillable = ['class_type','class_id','file_path'];
}
