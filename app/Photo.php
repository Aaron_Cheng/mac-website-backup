<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable=['id','title','subtitle','path','color','created_at','updated_at'];
}
