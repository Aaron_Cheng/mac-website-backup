<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntroduceRule extends Model
{
    protected $fillable = ['id','title','classification','path','created_at','updated_at'];
}
