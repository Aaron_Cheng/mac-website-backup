<!DOCTYPE html>
<html lang="en">

  <head>

    <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>中央大學微課程系統</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/news/modern-business.css')}}" rel="stylesheet">



  </head>

  <body>

   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #FFF; height: 55px;">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"><img src="http://www.ncu.edu.tw/assets/thumbs/pic/df1dfaf0f9e30b8cc39505e1a5a63254.png" height="25" width="25" ><b>自主學習微課程系統</b></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/News') }}"><b>最新公告</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ asset('/introduce') }}"><b>簡介</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/achievement') }}"><b>成果展示</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/record') }}"><b>課程查詢</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/video/video') }}"><b>課程影音</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/application') }}"><b>開課單位登入</b></a>
            </li>
             @if (Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/authrize') }}"><b>管理員登入</b></a>
                    </li>
                    @else
                    <ul class="nav navbar-nav navbar-right ml-auto">
                      <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light nav-link js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
                          <b>管理員 您好</b>
                          <span class="caret"></span>
                          <small class="tips"></small>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url('/authrize/menu') }}" class=" waves-effect waves-light nav-link js-scroll-trigger"><i class="fa fa-user" aria-hidden="true"> </i>功能主頁</a>
                          </li>
                          <li class="page-scroll navbtn">
                            <a class=" nav-link js-scroll-trigger " href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-formm').submit();">
                              登出
                            </a>
                            <form id="logout-formm" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </li>
                    </ul>

              @endif

          </ul>
        </div>
      </div>
    </nav>

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <input type="hidden" name="{{$j=1}}" value="">
          @foreach($photo as $photos)
          <li data-target="#carouselExampleIndicators" data-slide-to="{{$j}}" class="@if($j==1) active @endif img"></li>
          <input type="hidden" name="{{$j=$j+1}}" value="{{$j}}">
          @endforeach
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->

          <input type="hidden" name="{{$i=1}}" value="">
          @foreach($photo as $photos)
            <div class="carousel-item @if($i==1) active @endif" style="background-image: url('/photo/{{$photos->path}}' )">
              <div class="carousel-caption d-none d-md-block" >
                <h3 style="color:#{{$photos->color}}">{{$photos->title}}</h3>
                <p style="color:#{{$photos->color}}">{{$photos->subtitle}}</p>
              </div>
            </div>
            <input type="hidden" name="{{$i=$i+1}}" value="{{$i}}">
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">
    <br>
      <center>
        <h2 class="my-4">最新公告</h2>
      </center>
    <br>
    <div class="container">
<div class="row">
  <div class="col-md-1" style="text-align:center">
    標記
  </div>
  <div class="col-md-2" style="text-align:center">
    發布時間
  </div>
  <div class="col-md-7" style="text-align:center">
    公告標題
  </div>
  <div class="col-md-2" style="text-align:center">
    點閱人數
  </div>
</div>
</div>
      <div class="list-group " style="overflow-y:scroll;height:400px;" >

        <script type="text/javascript">
        $(document).ready(function(){
            $(".newsbar").click(function(){
              $.ajaxSetup({
                headers: {
                  'X-XSRF-TOKEN': decodeURIComponent(/XSRF-Token=([^;]*)/ig.exec(document.cookie)[1])
                }
                });
              var id = $(this).attr('news_num');
                $.get("/News/counter/"+id,
                {
                },
                function(data,status){
                    $('.counter'+id).text(data);
                });
                    //console.log($(this).children().find('.counter').text());
            });
        });
        </script>

        @foreach ($news as $news)
        <div>
        <a data-toggle="modal" data-target="#News{{$news->id}}" news_num="{{$news->id}}" class="newsbar">

          <span class="list-group-item" class="newsbar_color" >
      <div class="row">
        <div class="col-md-1" style="text-align:center">
          @if ($news->ontop==1)
            <p  class="label warning" style="display:inline;">置頂</p><p  style="display:inline;">&nbsp;&nbsp;&nbsp;</p>
          @elseif($news->important==1)
            <p  class="label danger" style="display:inline;">重要</p><p  style="display:inline;">&nbsp;&nbsp;&nbsp;</p>
          @else
            <p style="display:inline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
          @endif
        </div>
        <div class="col-md-2" style="text-align:center">
          <?php

              $timestamp = strtotime($news->created_at) + 8*60*60;
              $time = date('Y-m-d', $timestamp);
            ?>
          {{$time}}
        </div>
        <div class="col-md-7" style="text-align:left">
          {{$news->title}}
        </div>
        <div class="col-md-2" style="text-align:center">
          <?php if ($news->important==1): ?>
          <?php endif; ?>
          <span class="counter{{$news->id}}">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp{{$news->count}}</span>
        </div>
        </span>
        </div>
      </div>



        </a>

          <div class="modal fade" id="News{{$news->id}}" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="margin:0px auto;">{{$time}} {{$news->title}}</h4>
                    <span class="modal-title" style="text-align: right;"><strong>公告</strong></span>
                  </div>
                  <div class="modal-body">
                    <ul style="list-style-type: none;">
                      <h6 style="margin:0px auto;text-align: center;">{{$news->subtitle}}</h6>
                      <hr>
                     {!!$news->content!!}
                   </ul>
                  </div>
                  <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">關閉</button>
                  </div>
                </div>
              </div>
          </div>


        @endforeach
      </div>


      <!-- Marketing Icons Section -->
      <!-- <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">台灣聯合大學系統開放系統學校學生跨校申請修讀學分學程公告</h4>
            <div class="card-body">
              <p class="card-text">台灣聯合大學系統開放系統學校學生跨校申請修讀學分學程公告</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-info">更多資訊</a>
            </div>
          </div>
        </div> -->
        <!-- <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">歡迎同學申請</h4>
            <div class="card-body">
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis ipsam eos, nam perspiciatis natus commodi similique totam consectetur praesentium molestiae atque exercitationem ut consequuntur, sed eveniet, magni nostrum sint fuga.</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-info">Learn More</a>
            </div>
          </div>
        </div> -->
        <!-- <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Card Title</h4>
            <div class="card-body">
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-info">Learn More</a>
            </div>
          </div>
        </div>
      </div> -->
      <!-- /.row -->



      <br>
      <br>

      <!-- Portfolio Section -->
      <center>
        <h2>最新課程</h2>
      </center>
      <br>

      <div class="row">



      <section class="p-0" id="portfolio">
      @if(isset($module_class))
      <div class="container-fluid">
        <div class="row no-gutter popup-gallery">
          @foreach ($module_class as $module_classes)
          <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src={{url('img/news_class/'.array_search($module_classes->field, $imgArray).'.jpg')}}></a>
            <div class="card-body" style="overflow-y:scroll;height:300px;">
              <h4 class="card-title">
                <a href="#">{{$module_classes->name}}</a>
              </h4>
              <p class="card-text" >{{$module_classes->class_intro}}</p>
            </div>
          </div>
        </div>
          @endforeach

          @if(count($module_class)<6)
          @for($i=0 ; $i<3-(count($module_class));$i++)
          <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="https://africanzebra.files.wordpress.com/2012/11/zebra-6-700-x-400.jpg" alt=""></a>
            <div class="card-body" style="overflow-y:scroll;height:300px;">
              <h4 class="card-title">
                <a href="#">模組課程</a>
              </h4>
              <p class="card-text">模組課程介紹</p>
            </div>
          </div>
        </div>
          @endfor
          @endif

          <!-- @foreach ($single_class as $single_classes)
          <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src={{url('img/news_class/'.array_search($single_classes->field, $imgArray).'.jpg')}}></a>
            <div class="card-body" style="overflow-y:scroll;height:300px;">
              <h4 class="card-title">
                <a href="#">{{$single_classes->name}}</a>
              </h4>
              <p class="card-text">{{$single_classes->class_intro}}</p>
            </div>
          </div>
        </div>
          @endforeach

          @if(count($single_class)<3)
          @for($i=0 ; $i<3-(count($single_class));$i++)
          <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="https://orig00.deviantart.net/244a/f/2010/225/f/2/the_world__700_by_400_by_hecatologue.png" alt=""></a>
            <div class="card-body" style="overflow-y:scroll;height:300px;">
              <h4 class="card-title">
                <a href="#">單獨課程</a>
              </h4>
              <p class="card-text">單獨課程介紹</p>
            </div>
          </div>
        </div>
          @endfor
          @endif -->

        </div>


      @else
      <p>目前無成果可顯示!</p>
      @endif

    </section>
    </div>
      <!-- /.row -->

      <!-- Features Section -->
      <!-- <div class="row">
        <div class="col-lg-6">
          <h2>Modern Business Features</h2>
          <p>The Modern Business template by Start Bootstrap includes:</p>
          <ul>
            <li>
              <strong>Bootstrap v4</strong>
            </li>
            <li>jQuery</li>
            <li>Font Awesome</li>
            <li>Working contact form with validation</li>
            <li>Unstyled page elements for easy customization</li>
          </ul>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
        </div>
        <div class="col-lg-6">
          <img class="img-fluid rounded" src="http://placehold.it/700x450" alt="">
        </div>
      </div> -->
      <!-- /.row -->

      <hr>

      <!-- Call to Action Section -->


    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white footer_override">版權所有 &copy; 大數據暨程式設計研究社 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/news/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/news/popper/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/news/bootstrap/js/bootstrap.min.js') }}"></script>

  </body>
<style type="text/css">

body{
  font-family:"Microsoft JhengHei";
}

nav{
  font-size: large;
}
.img {
        max-width:450px;
        myimg:expression(onload=function(){
        this.style.width=(this.offsetWidth > 250)?"250px":"auto"});
      }
.fixed-top {
    position: fixed;
    top: 0;
    right: 0;
    left: 0;
    z-index: 1030;
}
.list-group-item{
    color: #212529;
    font-weight: bold;
}
.footer_override{
  color: #f05f40 !important;
  margin: 0!important;
  font-family: "Microsoft JhengHei";
  font-size: 18px;
  font-weight: bold!important;
  line-height: 1.5;
}
.danger {background-color: #f44336;
        font-size:13px;
        padding:1px;
        color:white;
        border-radius:3px;
  } /* Red */

.warning {background-color: #f0ad4e;
          font-size:13px;
          padding:1px;
          color:white;
          border-radius:3px;
    } /* Red */

.newsbar_color{/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f2f6f8+93,f2f6f8+93,e07408+100,fcfffc+100,e0eff9+100,f25e09+102 */
background: rgb(242,246,248); /* Old browsers */
background: -moz-linear-gradient(-45deg,  rgba(242,246,248,1) 93%, rgba(242,246,248,1) 93%, rgba(224,116,8,1) 100%, rgba(252,255,252,1) 100%, rgba(224,239,249,1) 100%, rgba(242,94,9,1) 102%); /* FF3.6-15 */
background: -webkit-linear-gradient(-45deg,  rgba(242,246,248,1) 93%,rgba(242,246,248,1) 93%,rgba(224,116,8,1) 100%,rgba(252,255,252,1) 100%,rgba(224,239,249,1) 100%,rgba(242,94,9,1) 102%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(135deg,  rgba(242,246,248,1) 93%,rgba(242,246,248,1) 93%,rgba(224,116,8,1) 100%,rgba(252,255,252,1) 100%,rgba(224,239,249,1) 100%,rgba(242,94,9,1) 102%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#f25e09',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}
</style>
</html>
