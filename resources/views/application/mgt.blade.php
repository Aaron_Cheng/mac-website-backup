@extends('layouts.app')

@section('title', '管理已開設課程')

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">
  <br>
  <!-- 登入歡迎訊息 -->
  <div class="alert alert-info alert-dismissable  fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span class="glyphicon glyphicon-user"></span>
    <strong> {{$user->unitname}} </strong>
    您好 ! 歡迎登入 !
  </div>
  <p >
  <a href="{{ url('/application/module') }}">
    <button type="button" class="btn btn-success btn-sm" id="new_class"><span class="glyphicon glyphicon-plus"></span> 新增微課程</button>
  </a>
  <a href="{{ url('/application/contralAchievement') }}" data-toggle="tooltip" title="將成果上傳">
    <button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span>新增成果上傳</span></button>
  </a>
  </p>
  <div class="well form-horizontal">
    <fieldset>

      <!-- title -->
      <legend>
        <b>管理已開設課程</b>

      </legend>

      <!-- course table -->
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>開課學期</th>
              <th>課程名稱</th>
              <th>上課時數</th>
              <th>認證時數</th>
              <th>
                <a href="{{ url('/application/edit_pwd') }}">
                  <button type="button" class="btn btn-basic btn-sm" id="edit_pwd"><span class="glyphicon glyphicon-cog"></span> 編輯密碼 </button>
                </a>
              </th>
              <th>
                <a href="{{ url('/application/logout') }}">
                  <button type="button" class="btn btn-primary btn-sm" id="logout"><span class="glyphicon glyphicon-off"></span> 登出</button>
                </a>
              </th>

            </tr>
          </thead>
          <tbody>

            @foreach ($module_classes as $module_class)
            <tr class="info">
              <td>{{$module_class->term}}</td>
              <td>{{$module_class->name}}</td>
              <td>{{$module_class->class_hr}}</td>
              <td>{{$module_class->auth_hr}}</td>
              
              <!--班級管理-->
              <td>
                <a href="{{ url('/application/classmanage/微課程') }}/{{$module_class->id}}/{{$admin_id}}">
                  <button type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-edit"></span> 管理班級
                  </button>
                </a>
              </td>
              <!-- view -->
              <td>
                <a href="{{ url('/application/view_module') }}/{{$module_class->id}}">
                  <button type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-eye-open"></span> 查看詳情
                  </button>
                </a>
              </td>
              <!-- edit -->
              <td>
                <a href="{{ url('/application/edit_module') }}/{{$module_class->id}}">
                  <button type="button" class="btn btn-warning btn-sm">
                    <span class="glyphicon glyphicon-pencil"></span> 編輯
                  </button>
                </a>
              </td>
              <!-- del -->
              <td>
                <a href="#" data-toggle="modal" data-target="#deletemodule{{$module_class->id}}">
                  <button type="button" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-trash"></span> 刪除
                  </button>
                </a>
              </td>
              <td>
              @if (isset($module_class->file_path))
                <a href="{{ url('/application/module/delfile/'.$module_class->cid) }}">
                  <button type="button" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-trash"></span> 刪除附件
                  </button>
                </a>
              @else
                <a href="#" data-toggle="modal" data-target="#addfilemodule{{$module_class->id}}">
                  <button type="button" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-plus"></span> 新增附件
                  </button>
                </a>
              @endif
              </td>
            </tr>
            <!-- delete module -->
            <div class="modal fade" id="deletemodule{{$module_class->id}}" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">確認刪除?</h4>
                    </div>
                    <div class="modal-body">
                        <p>刪除的動作將無法復原</p>
                    </div>
                    <div class="modal-footer">
                        <center>
                          <a href="{{asset('/application/deleteModule')}}/{{$module_class->id}}">
                            <button type="submit" class="btn btn-danger">
                              <span class="glyphicon glyphicon-trash"></span> 確認刪除
                            </button>
                          </a>
                        </center>
                    </div>
                  </div>
                </div>
              </div>


            @endforeach

          </tbody>
        </table>
      </div>

    </fieldset>
  </div>
</div>

@foreach ($module_classes as $module_class)
<!-- add file module -->

<div class="modal fade" id="addfilemodule{{$module_class->id}}" role="dialog">

  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">新增附件</h4>
      </div>
      <form action="{{url('/application/module/addfile/'.$module_class->id)}}" method="POST" enctype="multipart/form-data">
        <div class="modal-body">

            {{csrf_field()}}
            <input type="file" name="file">

        </div>

        <div class="modal-footer">
            <center>
                <button type="submit" class="btn btn-danger">
                  <span class="glyphicon glyphicon-plus"></span> 確認新增
                </button>
            </center>
        </div>
        </form >
    </div>
  </div>
</div>

@endforeach
@endsection

@section('css')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<style>
#new_class {
  /*float: right;*/
  /*background-color:#33cccc;*/
  /*border-color: #33cccc;*/
  /*transition-duration: 0.4s;*/
}
#new_class:hover {
  /*float: right;*/
  /*color:#ffffff;*/
  /*background-color:#248f8f;*/
  /*border-color: #248f8f;*/
}
#logout {
  /*float: right;*/
  background-color:#33cccc;
  border-color: #33cccc;
  transition-duration: 0.4s;
}
#logout:hover {
  /*float: right;*/
  color:#ffffff;
  background-color:#248f8f;
  border-color: #248f8f;
}
#edit_pwd{
  /*float: right;*/
  background-color: #808080;
  border-color: #808080;
  color: #ffffff;
}
#edit_pwd:hover{
  /*float: right;*/
  background-color:#737373;
  border-color: #595959;
  color: #ffffff;
}
#act{
  float:right;
}
*{
  font-family: Microsoft JhengHei;
}
</style>
<style>
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }
    </style>
@endsection

@section('js')
<script>

</script>
@endsection
