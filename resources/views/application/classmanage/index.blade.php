@extends('layouts.app')

@section('title', '管理班級')

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">
  <br>
  <!-- 登入歡迎訊息 -->
  <div class="alert alert-info alert-dismissable  fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span class="glyphicon glyphicon-user"></span>
    <strong>歡迎來到課程管理區</strong>
  </div>
  @if ($success = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>上傳成功{{Session::get('name')}}</strong>
    </div>
  @endif
  @if ($success = Session::get('d_info'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>刪除成功</strong>
    </div>
  @endif
  @if (session('alert'))
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('alert') }}
    </div>
@endif

  <p>
    <button type="submit" onclick="goBack()" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-left"></span> 回上一頁</button>
  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#eModal"><span class="glyphicon glyphicon-plus"></span>上傳本班修課名單</button>
  <a href="{{ asset('/application/classmanage/delete_user/'.$class_id ) }}">
    <button type="submit" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-exclamation-sign"></span> 傳錯了!?點我刪除全部修課名單</button>
  </a>
  </p>
  <div class="modal fade" id="eModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <form class="form-horizontal" method="POST" action="{{ asset('/excel/import/'.$class_id.'/'.$admin_id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">*請選擇excel檔</h4>
          </div>
          <div class="modal-body">
            <code>!!注意 請按照格式上傳&nbsp;<a href="/exports/example2.xls"><b><span class="glyphicon glyphicon-download"></span>點我下載範例檔案</b></a></code>
            <br><code>若excel檔中有分頁也可能會出現錯誤</code>
            <br><br>
            <button type="button" class="btn btn-success btn-sm" >
              <input id="csv_file" type="file" class="form-control" name="file" required>
            </button>
          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-warning btn-sm"></span>確認上傳</span></button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="well form-horizontal">
    <fieldset>

      <!-- title -->
      <legend>
        <b>{{ $class->name }}</b>

      </legend>

      <!-- course table -->
        {{ csrf_field() }}
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>學期別</th>
                  <th>課號</th>
                  <th>必/選修</th>
                  <th>學生學號</th>
                  <th>是否通過</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if(is_null($stu))
                    <kbd>尚未新增資料!&nbsp; 想新增修課名單請按上面的綠色按鈕<b>"上傳本班修課名單"</b></kbd>
                @else
                  @foreach($stu as $stus)
                    <tr>
                      <td>{{$class->term}}</td>
                      <td>{{ $class->number }}</td>
                      <td>{{ $class->obligatory }}</td>
                      <td>{{ $stus->acct }}</td>
                      <td>@if($stus->is_pass == '1' )
                            <span class="glyphicon glyphicon-ok-circle" style="color:#00cc99;font-size:20px"></span>&nbsp;是
                          @else
                            <span class="glyphicon glyphicon-remove-circle" style="color:#ff1a1a;font-size:20px"></span>&nbsp;否
                          @endif
                      </td>

                      <td></td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
        </fieldset>
      </div>
  </div>
</div>




@endsection
@section('js')
<script type="text/javascript">
function goBack() {
    window.history.back();
}

</script>
@endsection
