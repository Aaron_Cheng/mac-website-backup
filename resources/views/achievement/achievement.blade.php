<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>中央大學微課程 | 成果展示</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/achievement/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('css/achievement/business-frontpage.css')}}" rel="stylesheet">


    <style>
    .card-title{
      color:#f05f40;
    }
    .modal-title{
      color:#f05f40;
    }
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }

    </style>

  </head>

  <body id="page-top">

   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #FFF; height: 55px;">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"><img src="http://www.ncu.edu.tw/assets/thumbs/pic/df1dfaf0f9e30b8cc39505e1a5a63254.png" height="25" width="25" ><b>自主學習微課程系統</b></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/News') }}"><b>最新公告</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ asset('/introduce') }}"><b>簡介</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/achievement') }}"><b>成果展示</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/record') }}"><b>課程查詢</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/video/video') }}"><b>課程影音</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/application') }}"><b>開課單位登入</b></a>
            </li>
             @if (Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/authrize') }}"><b>管理員登入</b></a>
                    </li>
                    @else
                    <ul class="nav navbar-nav navbar-right ml-auto">
                      <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light nav-link js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
                          <b>管理員 您好</b>
                          <span class="caret"></span>
                          <small class="tips"></small>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url('/authrize/menu') }}" class=" waves-effect waves-light nav-link js-scroll-trigger"><i class="fa fa-user" aria-hidden="true"> </i>功能主頁</a>
                          </li>
                          <li class="page-scroll navbtn">
                            <a class=" nav-link js-scroll-trigger " href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-formm').submit();">
                              登出
                            </a>
                            <form id="logout-formm" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </li>
                    </ul>

              @endif

          </ul>
        </div>
      </div>
    </nav>

    <!-- Header with Background Image -->
    <header class="business-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          <br>
          <br>
          <br>
            <h1 class="display-3 text-center text-white mt-4 title">成果展示</h1>
          </div>
        </div>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">


      <br>
      <div class="row">
        <div class="col-md-9 my-4">
        </div>
        <div class="col-md-3 my-4">
          <button type="button" class="btn" onclick="openSearch()">查詢成果</button>
          <a href="{{url('/achievement/past')}}"><button type="button" class="btn" >歷史成果</button></a>
        </div>

        <div class="col-md-3 my-4">
        </div>
        <div class="col-md-8" id="search">
          <form class="form-horizontal" action="{{ url('achievement/search') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label class="control-label col-md-4">發表學期:</label>
              <div class="col-md-10">
                <input type="text" class="form-control" id="term" placeholder="ex.106-1" name="term">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">召集人:</label>
              <div class="col-md-10">
                <input type="text" class="form-control" id="gather_name" placeholder="請輸入召集人姓名" name="gather_name">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">社群類別:</label>

              <select class=" form-control search" style="margin-left: 15px;" name="field" id="field">
                <option>同領域(同院、系)</option>
                <option>跨領域(跨院、校)</option>
              </select>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">成果主題:</label>
              <div class="col-md-10">
                <input type="text" class="form-control" id="result_topic" placeholder="" name="result_topic">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">關鍵字:</label>
              <div class="col-md-10">
                <input type="text" class="form-control" id="keyword" placeholder="" name="keyword">
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-offset-2 col-md-10">
              <center>
                <button type="submit" class="btn btn-default" onclick="openResult()">查詢</button>
                <button type="button" class="btn" onclick="exitSearch()">關閉</button>
              </center>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- search result -->
      @if(isset($search_achievements))
      <div class="col-md-4 my-4" id="result">
        <h2 class="bold-text">查詢結果</h2>

      </div>
      @endif
      <div class="row">
        @if(isset($search_achievements))
          @if($search_achievements->isEmpty())
          <div class="col-md-4 my-4">
            <h4>查無結果!</h4>
          </div>
          @else
          @foreach ($search_achievements as $search_achievement)
          <div class="col-md-4 my-4" id="{{$search_achievement->id}}">
            <div class="card">
              <?php
              $string_example="{$search_achievement->accociate}";
              $string_example = str_replace('&feature=youtu.be',"",$string_example);
              $replace_example = str_replace('watch?v=',"embed/",$string_example);
               ?>
                <iframe class="card-body"  height="220" src="{{$replace_example}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
              <div class="card-body">
                <h4 class="card-title">{{$search_achievement->result_topic}}</h4>
                <p class="card-text"><?php echo mb_substr($search_achievement->result_intro,0,65,"utf-8"); if(mb_strlen($search_achievement->result_intro,"utf-8")>65){ echo "...."; } ?></p>
              </div>
              <div class="card-footer">
                <a href="{{url('/achievement/'.$search_achievement->id)}}" class="btn btn-primary">查看詳情</a>
              </div>
            </div>
          </div>

        @endforeach
        @endif
        @endif
      </div>
      <!-- /.row -->

      <!-- default -->

      <div class="col-md-4 my-4">
        <h2 class="bold-text">全部成果</h2>

      </div>

      <div class="row">
        @if(isset($achievements))
        @foreach ($achievements as $achievement)
        <div class="col-md-4 my-4" id="{{$achievement->id}}">
          <div class="card">
            <?php
            $string_example="{$achievement->accociate}";
            $string_example = str_replace('&feature=youtu.be',"",$string_example);
            $replace_example = str_replace('watch?v=',"embed/",$string_example);
             ?>
              <iframe class="card-body"  height="220" src="{{$replace_example}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>

            <div class="card-body">
              <h4 class="card-title">{{$achievement->result_topic}}</h4>
              <p class="card-text"><?php echo mb_substr($achievement->result_intro,0,65,"utf-8"); if(mb_strlen($achievement->result_intro,"utf-8")>65){ echo "...."; } ?></p>
            </div>
            <div class="card-footer">
              <a href="{{url('/achievement/'.$achievement->id)}}" class="btn btn-primary" >查看詳情</a>
            </div>
          </div>
        </div>
        @endforeach
        @endif
        @if(!isset($achievements))
        目前無結果可以顯示!
        @endif
      </div>
      <!-- /.row -->

    </div>
    <center>{{ $page->links('vendor.pagination.default') }}</center>
    <!-- /.container -->

    @include('layouts.footer')

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/achievement/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/achievement/popper/popper.min.js')}}"></script>
    <script src="{{asset('vendor/achievement/bootstrap/js/bootstrap.min.js')}}"></script>

    <script>

    function openSearch() {

        $("#search").fadeIn("slow");

    }
    function exitSearch() {
      $("#search").fadeOut("slow");
    }

    </script>
  </body>

</html>
