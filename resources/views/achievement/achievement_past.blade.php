<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>中央大學微課程 | 成果展示</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/achievement/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('css/achievement/business-frontpage.css')}}" rel="stylesheet">


    <style>
    .card-title{
      color:#f05f40;
    }
    .modal-title{
      color:#f05f40;
    }
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }

    </style>

  </head>

  <body id="page-top">

   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #FFF; height: 55px;">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"><img src="http://www.ncu.edu.tw/assets/thumbs/pic/df1dfaf0f9e30b8cc39505e1a5a63254.png" height="25" width="25" ><b>自主學習微課程系統</b></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/News') }}"><b>最新公告</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ asset('/introduce') }}"><b>簡介</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/achievement') }}"><b>成果展示</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/record') }}"><b>課程查詢</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/video/video') }}"><b>課程影音</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/application') }}"><b>開課單位登入</b></a>
            </li>
             @if (Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/authrize') }}"><b>管理員登入</b></a>
                    </li>
                    @else
                    <ul class="nav navbar-nav navbar-right ml-auto">
                      <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light nav-link js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
                          <b>管理員 您好</b>
                          <span class="caret"></span>
                          <small class="tips"></small>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url('/authrize/menu') }}" class=" waves-effect waves-light nav-link js-scroll-trigger"><i class="fa fa-user" aria-hidden="true"> </i>功能主頁</a>
                          </li>
                          <li class="page-scroll navbtn">
                            <a class=" nav-link js-scroll-trigger " href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-formm').submit();">
                              登出
                            </a>
                            <form id="logout-formm" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </li>
                    </ul>

              @endif

          </ul>
        </div>
      </div>
    </nav>

    <!-- Header with Background Image -->
    <header class="business-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          <br>
          <br>
          <br>
            <h1 class="display-3 text-center text-white mt-4 title">過去成果</h1>
          </div>
        </div>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">


      <br>

      <!-- default -->
      <div class="col-md-4 my-4">
        <h2 class="bold-text">歷史成果</h2>
    </div>


        @if(isset($achievements))
        <?php
            $year=$achievements[0]->term;
            echo $year;
        ?>
        <div class="row">
        @foreach ($achievements as $achievement)
            @if($year!=$achievement->term)
            </div>
            <?php
                $year = $achievement->term;
                echo $year;
            ?>
            <div class="row">
            @endif
            <div class="col-md-4 my-4" id="{{$achievement->id}}">
            <div class="card">
                <?php
                $string_example="{$achievement->accociate}";
                $string_example = str_replace('&feature=youtu.be',"",$string_example);
                $replace_example = str_replace('watch?v=',"embed/",$string_example);
                ?>
                <iframe class="card-body"  height="220" src="{{$replace_example}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>

                <div class="card-body">
                <h4 class="card-title">{{$achievement->result_topic}}</h4>
                <p class="card-text"><?php echo mb_substr($achievement->result_intro,0,65,"utf-8"); if(mb_strlen($achievement->result_intro,"utf-8")>65){ echo "...."; } ?></p>
                </div>
                <div class="card-footer">
                <a href="{{url('/achievement/'.$achievement->id)}}" class="btn btn-primary" >查看詳情</a>
                </div>
            </div>
            </div>
        @endforeach
        </div>
        @endif
        @if(!isset($achievements))
        目前無結果可以顯示!

        @endif

      <!-- /.row -->

    </div>

    <!-- /.container -->

    @include('layouts.footer')

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/achievement/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/achievement/popper/popper.min.js')}}"></script>
    <script src="{{asset('vendor/achievement/bootstrap/js/bootstrap.min.js')}}"></script>

    <script>

    function openSearch() {

        $("#search").fadeIn("slow");

    }
    function exitSearch() {
      $("#search").fadeOut("slow");
    }

    </script>
  </body>

</html>
