<!-- Navigation -->

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom" style="background-color:#fff;">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <a class="navbar-brand" href="{{ url('/') }}" style="color:#f05f40 ;"><span>&nbsp;&nbsp;<img src="http://www.ncu.edu.tw/assets/thumbs/pic/df1dfaf0f9e30b8cc39505e1a5a63254.png" height="25" width="25" ></span><b>自主學習微課程系統</b></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="myNavbar" >
                <ul class="nav navbar-nav navbar-right ">
                    <li class="" >
                        <a href="{{ url('/News') }}" style="color:#f05f40 ; font-size:18px"><b>最新公告</b></a>
                    </li>

                    <li class="">
                        <a href="{{ url('/introduce') }}" style="color:#f05f40 ;font-size:18px"><b>簡介</b></a>
                    </li>
                    <li class="">
                        <a href="{{ url('/achievement') }}" style="color:#f05f40 ;font-size:18px"><b>成果展示</b></a>
                    </li>
                    <li class="">
                        <a href="{{ url('/record') }}" style="color:#f05f40 ;font-size:18px"><b>課程查詢</b></a>
                    </li>
                    <li class="">
                        <a href="{{ url('/video') }}" style="color:#f05f40 ;font-size:18px"><b>課程影音</b></a>
                    </li>
                    <li class="">
                        <a href="{{ url('/application') }}" style="color:#f05f40 ;font-size:18px"><b>開課單位登入</b></a>
                    </li>

                    @if (Auth::guest())
                    <li class="page-scroll navbtn ">
                        <a href="{{ url('/authrize') }}" style="color:#f05f40 ;font-size:18px"><b>管理員登入</b></a>
                    </li>
                    @else
                    <ul class="nav navbar-nav navbar-right">
                      <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" role="button" aria-expanded="false" style="color:#f05f40 ;font-size:18px">
                          <b>管理員 您好</b>
                          <span class="caret"></span>
                          <small class="tips"></small>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url('/authrize/menu') }}" class=" waves-effect waves-light" style="color:#f05f40 ;font-size:20px"><i class="fa fa-user" aria-hidden="true"> </i><b>功能主頁</b></a>
                          </li>
                          <li class="page-scroll navbtn">
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-formm').submit();" style="color:#f05f40 ;font-size:20px">
                              <b>登出</b>
                            </a>
                            <form id="logout-formm" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </li>
                    </ul>

                    @endif

                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
