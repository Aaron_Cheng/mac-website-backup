{{-- Footer --}}

  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white footer_override">版權所有 &copy; 大數據暨程式設計研究社 2018</p>
    </div>
    <!-- /.container -->
  </footer>
