@extends('layouts.app')

@section('title', '課程查詢')

@section('content')
	<div class="container">
		<!--標題-->
		<div class="page-header">
			<center>課程查詢</center>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-1">

			</div>
			<div class="col-sm-10">
				<div class="row2">
					<center >
						<ul class="nospace group services" >
							<li class="one_quarter">
								<article><a href="{{ url('/record/當學期微課程') }}"><img src="/img/as2.jpg" alt="" width="99%"></a>
									<h6 class="heading font-x1"><a href="{{ url('/record/當學期微課程') }}">當學期微課程</a></h6>
								</article>
							</li>
							<li class="one_quarter" >
								<article>
								</article>
							</li>
							<li class="one_quarter">
								<article><a href="{{ url('/search') }}"><img src="/img/as21.jpg" alt="" width="99%">
									<h6 class="heading font-x1"><a href="{{ url('/search') }}">歷年微課程</a></h6>
								</article>
							</li>
						</ul>
					</center>
				</div>

			</div>
			<div class="col-sm-1">

			</div>
		</div>


	 </div>

@endsection

@section('css')
<link rel="stylesheet" href="/css/search.css">
<style type="text/css">
	body{
		font-family: 微軟正黑體;
	}
	nav{
		font-size: large; !important;
		font-family:"Microsoft JhengHei"; !important
	}

		.page-header{
			font-size: 35px;
			margin-top: 50px;
			/*font-weight: bold;*/
		}
		/*查詢歷年課程字體*/
		font{
			font-size: 20px;
		}

		/*內容*/
		.content{
			margin-left: 4%;
		}

		/*Modal標題字體*/
		h4{
			font-weight: bold;
		}
		.footer_override{
			color: #f05f40 !important;
			margin: 0!important;
			font-family: "Microsoft JhengHei";
			font-size: 18px;
			font-weight: bold!important;
			line-height: 1.5;
		}
</style>

@endsection

@section('js')
<script>
 $(document).ready(function(){
		 $('[data-toggle="tooltip"]').tooltip();
 });
 </script>
@endsection
