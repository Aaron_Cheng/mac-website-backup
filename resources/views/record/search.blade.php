@extends('layouts.app')

@section('title', '課程搜尋')

@section('content')
	<div class="container">

		<!--圖片及標題-->
		<div class="bcg">
			<div style="line-height:500px;color:#fff;padding-top:180px">
				<center><h1>歷&nbsp;年&nbsp;課&nbsp;程</h1>
					<h5 style="font-style:oblique;padding-left:0%;color:#f2f1f2;letter-spacing:1px;">“我寧可做人類中有夢想和有完成夢想的願望的、最渺小的人，而不願做一個最偉大的、無夢想、無願望的人。” - 紀伯倫</h5>
				</center>
			</div>
	</div><br>

	<!-- 回上一頁button -->
	<div class="row" style="margin-bottom:15px;padding:15px">
		<div class="col-sm-1">
			<a href="{{url('/record')}}"><button class="btn btn-defaulte " type="button">回上一頁</button></a>
		</div>
	</div><hr>

		<!--回傳結果-->
		@if ($search_other = Session::get('search_other'))
			<div class="alert alert-block" style="background-color:#262626">
				<button type="button" class="close" data-dismiss="alert" style="color:#e65c00;">×</button>
				<center><h3 style="color:#e74c3c"><span class="glyphicon">&#xe003;</span>&nbsp;搜尋結果</h3></center>
				@if($search_other->isEmpty())
					<strong style="color:#e74c3c">查無結果...</strong>
				@else
					<tbody style="background-color:">
						<table class="table table-hover" >
							<tbody style="background-color:">
								@foreach ($search_other as $key2=>$other)
									<a href="{{ url('record/view_module/'.$other->id) }}" class="list-group-item">
										<h3><b>{{$key2+1}}、</b>{{ $other->name }}</h3>
										<h5><span class="glyphicon">&#xe008;</span>&nbsp;{{$other->teacher}}</h5>
									</a>
								@endforeach
							</tbody>
						</table>
					</tbody>
				@endif
			</div>
		@endif

		<!-- 微課程查詢輸入區段 -->
		<div>
			<ul class="nav nav-tabs" >
				<li class="active"><a data-toggle="tab" href="#menu1" >歷史查詢</a></li>
			</ul>

			<div class="tab-content tab-search" >
				<div id="menu1" class="tab-pane fade in active">
					<form  action=" {{ url('search2') }} " method="post">
						{{ csrf_field() }}
						<br><b>學期別:</b>
						<input class="form-control" type="text" name="term" placeholder="必填 ex:106-1 106-2" required><hr>
						<b>課程名稱:</b>
						<input class="form-control" type="text" name="name" placeholder="可不填"><hr>
						<b>查關鍵字:</b>
						<input class="form-control" type="text" name="tag" placeholder="可不填"><hr>
						<button type="submit" class="btn btn-warning">查詢</button><br><br>
					</form>
				</div>
			</div>
		</div><br>


	 </div>
@endsection

@section('css')

	<style type="text/css">
	.footer_override{
		color: #f05f40 !important;
		margin: 0!important;
		font-family: "Microsoft JhengHei";
		font-size: 18px;
		font-weight: bold!important;
		line-height: 1.5;
	}
	.tab-search{
		border-left:#d9d9d9 solid 1px;
		border-right:#d9d9d9 solid 1px;
		border-bottom:#d9d9d9 solid 1px;
		background-color:#fbfbfb;
		padding:3% 5% 3% 5%;
	}
	.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    cursor: default;
    background-color: #00264d;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
}
		.bcg {
		    /* The image used */
		    background-image: url("/img/record1.png");

		    /* Full height */
		    height: 400px;
				margin-left: -210px;
				margin-right:-173px;
		    /* Center and scale the image nicely */
		    background-position: center;
		    background-repeat: no-repeat;
		    background-size: cover;
		}
		
	.containercard {
	  max-width: 800px;
	  min-width: 640px;
	  margin: 0 auto;
	}



		.btn, .btn.inverse:hover{color:#fff; background-color:#e74c3c; border-color:#e74c3c;}
		.btn:hover, .btn.inverse{color:inherit; background-color:transparent; border-color:inherit;}
		*, *::before, *::after{transition:all .28s ease-in-out;}

		/*查詢歷年課程字體*/
		font{
			font-size: 20px;
		}

		/*內容*/
		.content{
			margin-left: 4%;
		}

		/*Modal標題字體*/
		h4{
			font-weight: bold;
		}
		.panel-heading{
			background-color: #da8b8b;
		}
	</style>
@endsection

@section('js')

@endsection
