@extends('layouts.app')

@section('title', $module_class->name)

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">

  <table>
    <tr>
      <th>課&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
      <th>{{$module_class->name}}</th>
    </tr>
    <tr>
      <td>開課學期</td>
      <td>{{$module_class->term}}</td>
    </tr>
    <tr>
      <td>課程名稱</td>
      <td>{{$module_class->name}}</td>
    </tr>
    <tr>
      <td>課程領域</td>
      <td>{{$module_class->field}}</td>
    </tr>
    <tr>
      <td>其他分類</td>
      <td>{{$module_class->other_field}}</td>
    </tr>
    <tr>
      <td>上課地點</td>
      <td>{{$module_class->location}}</td>
    </tr>
    <tr>
      <td>人數限制</td>
      <td>{{$module_class->limit}}</td>
    </tr>
    <tr>
      <td>講師姓名</td>
      <td>{{$module_class->teacher}}</td>
    </tr>
    <tr>
      <td>聯絡方式</td>
      <td>{{$module_class->email}}</td>
    </tr>
    <tr>
      <td>講師介紹</td>
      <td>{{$module_class->teacher_intro}}</td>
    </tr>
     <tr>
        <td>課程時數</td>
        <td>{{$module_class->class_hr}}</td>
      </tr>
      <tr>
         <td>認證時數</td>
         <td>{{$module_class->auth_hr}}</td>
       </tr>
       <tr>
          <td>課程簡介</td>
          <td>{{$module_class->class_intro}}</td>
        </tr>
        <!-- 以下三組為模組課程獨有 -->
        <tr>
           <td>課程目標</td>
           <td>{{$module_class->goal}}</td>
         </tr>
         <tr>
            <td>課程要求</td>
            <td>{{$module_class->claim}}</td>
          </tr>
          <tr>
             <td>其他</td>
             <td>{{$module_class->other}}</td>
           </tr>
           <tr>
             <td>關鍵字</td>
             <td>{{$module_class->keyword}}</td>
           </tr>
  </table>
      <br>
      @if (isset($file))
      <a href="{{url('/application/module/getfile/'.$module_class->id)}}">課程附件</a>
      @endif
      <!-- table -->
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>上課日期</th>
              <th>起始時間</th>
              <th>結束時間</th>
              <th>授課講師</th>
              <th>上課主題</th>
              <th>上課內容</th>
              <th>備註</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($modules as $module)
            <tr>
              <td>{{$module->date}}</td>
              <td>{{$module->start}}</td>
              <td>{{$module->end}}</td>
              <td>{{$module->teacher}}</td>
              <td>{{$module->unit}}</td>
              <td>{{$module->detail}}</td>
              <td>{{$module->comment}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
<br><br>



</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
table {
    border-collapse: collapse;
    width: 100%;
    margin-top: 50px;
}

td {
    text-align: left;
    padding: 8px;
}
th{
  font-size: 20px;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #f05f40;
    color: white;
}
</style>
<style>
#other_field{
  display: none;
}
</style>
@endsection

@section('js')
<script>
$(document).ready(function(){
  if($("#field").val() === "其他"){
    $("#other_field").show();
  }
});
</script>
@endsection
