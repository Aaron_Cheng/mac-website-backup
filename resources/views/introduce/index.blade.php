<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>中央大學微課程系統</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{asset('css/introduce/agency.min.css')}}" rel="stylesheet">
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #FFF; height: 55px;">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"><img src="http://www.ncu.edu.tw/assets/thumbs/pic/df1dfaf0f9e30b8cc39505e1a5a63254.png" height="25" width="25" ><b>自主學習微課程系統</b></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/News') }}" style="font-size:18px"> <b>最新公告</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ asset('/introduce') }}"style="font-size:18px"><b>簡介</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/achievement') }}"style="font-size:18px"><b>成果展示</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/record') }}"style="font-size:18px"><b>課程查詢</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/video/video') }}"style="font-size:18px"><b>課程影音</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/application') }}"style="font-size:18px"><b>開課單位登入</b></a>
            </li>
             @if (Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/authrize') }}"style="font-size:18px"><b>管理員登入</b></a>
                    </li>
                    @else
                    <ul class="nav navbar-nav navbar-right ml-auto">
                      <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light nav-link js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false" style="font-size:110%;">
                          <b>管理員 您好</b>
                          <span class="caret"></span>
                          <small class="tips"></small>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url('/authrize/menu') }}" class=" waves-effect waves-light nav-link js-scroll-trigger"><i class="fa fa-user" aria-hidden="true"> </i>功能主頁</a>
                          </li>
                          <li class="page-scroll navbtn">
                            <a class=" nav-link js-scroll-trigger " href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-formm').submit();">
                              登出
                            </a>
                            <form id="logout-formm" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </li>
                    </ul>

              @endif

          </ul>
        </div>
      </div>
    </nav>



    <!-- Header -->
    <header class="masthead" style="background-image: url('{{asset('img/introduce/title.jpg')}}');">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">簡介</div>
          <div class="intro-lead-in">About IMC</div>
          <!-- <a class="btn btn-xl js-scroll-trigger" href="#portfolio">Tell Me More</a> -->
        </div>
      </div>
    </header>



    <!-- Portfolio Grid -->
    <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
<!--           <div class="col-lg-12 text-center">
            <h2 class="section-heading">簡介</h2>
            <h3 class="section-subheading text-muted">About Mac-Website</h3>
          </div> -->
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/introduce/book1.jpg')}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>課程類型</h4>
              <p class="text-muted">Catagory</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/introduce/book2.jpg')}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>開課流程</h4>
              <p class="text-muted">Steps</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/introduce/question1.jpg')}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>相關問題</h4>
              <p class="text-muted">Q & A</p>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/introduce/rule1.jpg')}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>相關辦法與表格</h4>
              <p class="text-muted">Rules & Form</p>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/introduce/book5.jpg')}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>使用手冊</h4>
              <p class="text-muted">User Manual</p>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="{{asset('img/introduce/contact1.jpg')}}" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>聯絡人介紹</h4>
              <p class="text-muted">Contact Person</p>
            </div>
          </div>

        </div>
      </div>
    </section>



    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <h2>課程類型</h2>
                  <p class="item-intro text-muted">Catagory</p>
                  <hr>
                  <!-- Project Details Go Here -->
                  	@foreach($introduce_classtypes as $introduce_classtype)
          					  <?php
          						$str = $introduce_classtype['body'];
          						echo $str;
          					  ?>
          					@endforeach
          					<hr>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  	<h2>開課流程</h2>
                  	<p class="item-intro text-muted">Steps</p>
                  	<hr>
                  	<br>
                  	@foreach($introduce_classsteps as $introduce_classstep)
          						<?php
          							$str1 = $introduce_classstep['body'];
          							echo $str1;
          						?>
          					@endforeach
          					<hr>
                  	<button class="btn btn-primary" data-dismiss="modal" type="button">
                    	<i class="fa fa-times"></i>
                    	Close
                    </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>相關問題</h2>
                  <p class="item-intro text-muted">Q & A</p>
                  <hr>
                  	@foreach($introduce_questions as $introduce_question)
        					  <ul>
        						<div class="row">
        						  <div class="col-sm-10">
        							<li style="margin-left: 5%;">
        							  <p style="font-size: 15px;">
        							  	Question : {{$introduce_question->question}}
        							  </p>
        							  <br>
        							  <div id="{{$introduce_question->id}}" class="collapse">
        								Answer : {{$introduce_question->answer}}
        							  </div>
        							</li>
        						  </div>
        						  <div class="col-sm-2">
        							<button type="buttom" class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#{{$introduce_question->id}}">
                        			  <span class="glyphicon glyphicon-chevron-down"></span> 查看詳情
                      				</button>
        						  </div>
        						</div>
        						<br>
        						<br>
        					  </ul>
        					@endforeach
        				  <hr>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2><center>相關辦法與表格</center></h2>
                  <p class="item-intro text-muted"><center>Rules & Forms</center></p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/04-full.jpg" alt="">
                  <hr>

                  <div class="card">
                    <div class="card-header">老師相關</div>
                    <div class="card-body">
                        @foreach($introduce_rules as $introduce_rule)
                          @if($introduce_rule->classification == 'Teacher')
                          <li>
                            <a target="_blank" href="{{asset('/document/'.$introduce_rule->path)}}">{{$introduce_rule->title}}
                          </a>
                          </li><br>
                          @endif
                        @endforeach
                    </div> 
                  </div>

                  <br>

                  <div class="card">
                    <div class="card-header">學生相關</div>
                    <div class="card-body">
                        @foreach($introduce_rules as $introduce_rule)
                          @if($introduce_rule->classification == 'Student')
                          <li>
                            <a target="_blank" href="{{asset('/document/'.$introduce_rule->path)}}">{{$introduce_rule->title}}
                            </a>
                          </li><br>
                          @endif
                        @endforeach
                    </div> 
                  </div>

                  <br>

                  <div class="card">
                    <div class="card-header">法規相關</div>
                    <div class="card-body">
                        @foreach($introduce_rules as $introduce_rule)
                        @if($introduce_rule->classification == 'Rules')
                        <li>
                          <a target="_blank" href="{{asset('/document/'.$introduce_rule->path)}}">{{$introduce_rule->title}}
                        </a>
                        </li><br>
                        @endif
                      @endforeach
                    </div> 
                  </div>
                  <hr>
                  
                  <center>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>
                  </center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <h2>使用手冊</h2>
                  <p class="item-intro text-muted">User Manual</p>
                  <hr>
                  <!-- Project Details Go Here -->
                    @foreach($introduce_manuals as $introduce_manual)
                      <?php
                      $str2 = $introduce_manual['body'];
                      echo $str2;
                      ?>
                    @endforeach
                    <hr>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        <!-- Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content" style="text-align: center;">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <h2>聯絡人介紹</h2>
                  <p class="item-intro text-muted">Contact Person</p>
                  <hr>
                  <!-- Project Details Go Here -->
                    @foreach($introduce_contacts as $introduce_contact)
                      <?php
                      $str3 = $introduce_contact['body'];
                      echo $str3;
                      ?>
                    @endforeach
                    <hr>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="py-5">
      <div class="container">
        <p class="m-0 text-center text-white footer_override">版權所有 &copy; 大數據暨程式設計研究社 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('js/introduce/jquery.min.js')}}"></script>
    <script src="{{asset('js/introduce/popper.min.js')}}"></script>
    <script src="{{asset('js/introduce/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{asset('js/introduce/jquery.easing.min.js')}}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{asset('js/introduce/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('js/introduce/contact_me.js')}}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{asset('js/introduce/agency.min.js')}}"></script>

  </body>
  <style type="text/css">
  	.footer_override{
      color: #f05f40 !important;
      margin: 0!important;
      font-family: "Microsoft JhengHei";
      font-size: 18px;
      font-weight: bold!important;
      line-height: 1.5;
    }
  </style>
</html>
