<!DOCTYPE html>
<html>
    <head>
        <title>404</title>
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
                font-family: 微軟正黑體;
            }
            .button {
              background-color: #008CBA;
              border: none;
              color: white;
              padding: 15px 32px;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 16px;
              margin: 4px 2px;
              cursor: pointer;
          }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title"><img src="https://images2.laweekly.com/imager/oops-i-just-unplugged-san-diego-lets-fi/u/original/4238961/homer_d_oh_fox.jpg" width="25%">
                找不到您要的頁面</div>
                <a href="{{ url('/')}}"><button class="button ">點我回到微學分系統</button></a>

            </div>
        </div>
    </body>
</html>
