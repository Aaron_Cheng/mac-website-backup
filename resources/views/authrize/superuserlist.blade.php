@extends('layouts.app')

@section('title', '管理員帳號檢索')

@section('content')
<div class="container"><br><br>
  @if ($success = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>成功刪除{{Session::get('title')}}</strong>
    </div>
  @endif
<br>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">
    <legend><b>管理員帳號檢索</b></legend>
    <div style="padding:10px">
      <table class="table">
       <thead>
         <tr>
           <th>管理者姓名</th>
           <th>管理者帳號</th>
           <th>&nbsp&nbsp&nbsp&nbsp&nbsp操作</th>
         </tr>
       </thead>
       <tbody>
         @foreach($users as $users)
         <tr>
           <td>{{ $users->name }}</td>
           <td>{{ $users->email }}</td>
           <div class="form-group">
           <td>
                <div class="col-sm-4 col-md-4">
                   <form action="{{ asset('authrize/menu/superuserList/'.$users->id) }}" method="POST">
            								{!! csrf_field() !!}
            								{!! method_field('DELETE') !!}
            				<button type="submit" class="btn btn-danger">
            				<span class="glyphicon glyphicon-trash"></span>刪除
            				</button>
            			</form>
                </div>
          </td>
          </div>


         </tr>
         @endforeach
       </tbody>
      </table>
    </div>




</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('js')
@endsection
