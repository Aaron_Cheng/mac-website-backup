@extends('layouts.app')

@section('title','開課流程編輯')

@section('content')
<!-- 顯示 -->
<div class="panel-group">
  <div class="panel panel-info" style="margin-top: 5%;">
    <div class="panel-heading"><center>開課流程編輯</center></div>
    <div class="panel-body">
      @foreach($introduce_classsteps as $introduce_classstep)
        <?php
          $str = $introduce_classstep['body'];
            echo $str;
          ?>
      @endforeach
    </div>
  </div>
</div>
<!-- 編輯CKediter -->
  <div class="panel panel-info" style="margin-bottom:  15%;">
    <div class="panel-heading"><center>輸入開課流程</center></div>
    <div class="panel-body">
       @foreach($introduce_classsteps as $introduce_classstep)
        <form action="{{ asset('/authrize/menu/introduce/StepEdit/'.$introduce_classstep->id) }}" method="post">
          {{ csrf_field() }}
          <textarea id="ckeditor1" class="ckeditor" name="editor1">{{ $introduce_classstep -> body }}</textarea>
            <script type="text/javascript">
              CKEDITOR.replace( 'ckeditor1' );
            </script>
            <br>
            <center>
              <button type="submit" class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-ok"></span> 編輯完成
              </button>
              <button type="button" class="btn btn-info btn-sm">
                <a href="{{ url('/authrize/menu/introduce') }}" style="color:#fff;">
                <span class="glyphicon glyphicon-ok"></span> 返回管理者簡介頁面
                </a>
              </button>
            </center>
        </form>
      @endforeach
    </div>
  </div>
</div>

@endsection

@section('css')
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
@endsection
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

@section('js')
  <script src="{{asset('/ckeditor/ckeditor/ckeditor.js')}}"></script>
@endsection
