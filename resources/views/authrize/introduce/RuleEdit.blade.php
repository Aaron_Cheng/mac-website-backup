@extends('layouts.app')

@section('title', '相關法規與表格')

@section('content')

<div class=""><br>
      <br><h2 style="border-left:solid 2px #e6b3b3">&nbsp;相關法規與表格修改</h2><br>
       <form  enctype="multipart/form-data" action="{{ asset('/authrize/menu/introduce/RuleEdit') }}" method="post">
        {{ csrf_field() }}
         <div id="filetype">
           <h4>檔名: </h4><input type="text" name="title" class="form-control"><br>
           <h4>類別: </h4><select name="classification">
                          　<option value="Teacher">老師</option>
                          　<option value="Student">學生</option>
                            <option value="Rules">法規</option>
                          </select><br><br>
           <h4>檔案:  (上傳的檔案名稱請用英文)</h4>
           <div class="fileupload fileupload-new" data-provides="fileupload">
							<span class="btn btn-primary btn-file"><span class="fileupload-new"></span>
								<input type="file" name="rule"/></span>
							<span class="fileupload-preview"></span>
						</div> <br>
           <div class="row">
             <div class="col-sm-6">
             </div>
             <div class="col-sm-6">
               {{ csrf_field() }}
               <button type="submit" class="btn btn-success">確認</button>
             </div>
            </div>
         </div>
       </form>
    </div>
<!--     <script src="/js/news/jscolor.js"></script>
    <script type="text/javascript">
      $('#color').colorpicker({});
      $(document).ready(function(){
          $("#show_filetype").click(function(){
              $("#articletype").hide(700);
              $("#filetype").show(700);
          });
      });

      $(document).ready(function(){
          $("#show_articletype").click(function(){
              $('#articletype').css('display','').show(700);

              $("#filetype").hide(700);
          });
      });

    </script> -->

@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('js')
@endsection
