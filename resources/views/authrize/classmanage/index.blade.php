@extends('layouts.app')

@section('title', '選擇開課單位')

@section('content')

<div class="container"><br><br>
  <div class="row">
    <div class="container col-sm-1">
    </div>
    <div class="container col-sm-10">
      <legend><b>按開課單位查看學生修課狀況</b></legend>
      @if (session('no_file'))
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('no_file') }}
        </div>
      @endif
      <a href="{{ url('/authrize/menu') }}">
        <button type="submit" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-left"></span> 回上一頁</button>
      </a>
      <a href="{{ url('/authrize/export/all/'.$d_one) }}">
        <button type="submit" class="btn btn-default btn-sm">
          <span class="	glyphicon glyphicon-download-alt"></span> 匯出{{$d_one}}全部課程修課狀況
        </button>
      </a>
      <a href="{{ url('/authrize/export/all/'.$d_two) }}">
        <button type="submit" class="btn btn-default btn-sm">
          <span class="	glyphicon glyphicon-download-alt"></span> 匯出{{$d_two}}全部修課狀況
        </button>
      </a>
      <br><br>
      <!-- Button -->
      <div class="form-group">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>開課單位</th>
              <th colspan = 3>操作</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($applicants as $applicants)
            <tr class="success">
              <td>{{$applicants->unitname}}</td>
              <td>
                <a href="{{ url('authrize/menu/classmanage/') }}/{{$applicants->account}}">
                  <button type="button" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-pencil"></span> 查看該單位各班修課狀況
                  </button>
                </a>
              </td>
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>


    </div>
    <div class="col-sm-1">

    </div>

  </div>

</div>



@endsection

@section('css')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<style>
*{
  font-family: Microsoft JhengHei;
}
</style>
<style>
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }
    </style>
@endsection

@section('js')
<script>

</script>
@endsection
