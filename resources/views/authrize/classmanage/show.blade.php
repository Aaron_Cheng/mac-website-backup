@extends('layouts.app')

@section('title', '查看各班修課狀況')

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">
  <br>
  <a href="{{ url('/authrize/menu/classmanage') }}">
    <button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-left"></span> 回上一頁</button>
  </a>
  <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">
    <span class="glyphicon glyphicon-th-list"></span> 查看歷史資料
  </button>
  <br><br>
  <!-- Modal-->
  <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form  action=" {{ url('authrize/menu/classmanage/search/'.$user_account) }} " method="post" target="_blank">
          {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">查詢歷年資料</h4>
            </div>
            <div class="modal-body">
              <h4><b>學期別:</b></h4>
  						<input class="form-control" type="text" name="term" placeholder="必填 ex:107-1、107-2" required><br>
              <h4><b>課程名稱:</b></h4>
  						<input class="form-control" type="text" name="name" placeholder="可不填">
            </div>
            <div class="modal-footer">
              <button type="suubmit" class="btn btn-success">查詢</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  <div class="well form-horizontal">
    <fieldset>

      <!-- title -->
      <legend>
        <b>查看各班修課狀況</b>

      </legend>

      <!-- course table -->
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>學期別</th>
              <th>課程屬性</th>
              <th>課程名稱</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($module_classes as $module_class)
            <tr class="info">
              <td>{{$module_class->term}}</td>
              <td>套裝課程</td>
              <td>{{$module_class->name}}</td>
              <!--班級管理-->
              <td>
                <a href="{{ url('/authrize/classmanage/微課程') }}/{{$module_class->id}}/{{$admin_id}}" target="_blank">
                  <button type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-edit"></span> 查看學生修課狀況
                  </button>
                </a>
              </td>
              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>

    </fieldset>
  </div>
</div>


@endsection

@section('css')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<style>
#new_class {
  /*float: right;*/
  /*background-color:#33cccc;*/
  /*border-color: #33cccc;*/
  /*transition-duration: 0.4s;*/
}
#new_class:hover {
  /*float: right;*/
  /*color:#ffffff;*/
  /*background-color:#248f8f;*/
  /*border-color: #248f8f;*/
}
#logout {
  /*float: right;*/
  background-color:#33cccc;
  border-color: #33cccc;
  transition-duration: 0.4s;
}
#logout:hover {
  /*float: right;*/
  color:#ffffff;
  background-color:#248f8f;
  border-color: #248f8f;
}
#edit_pwd{
  /*float: right;*/
  background-color: #808080;
  border-color: #808080;
  color: #ffffff;
}
#edit_pwd:hover{
  /*float: right;*/
  background-color:#737373;
  border-color: #595959;
  color: #ffffff;
}
#act{
  float:right;
}
*{
  font-family: Microsoft JhengHei;
}
</style>
<style>
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }
    </style>
@endsection

@section('js')
<script>

</script>
@endsection
