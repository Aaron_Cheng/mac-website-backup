@extends('layouts.app')

@section('title', '管理班級')

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">
  <br>
  <!-- 登入歡迎訊息 -->
  <div class="alert alert-info alert-dismissable  fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span class="glyphicon glyphicon-user"></span>
    <strong>歡迎來到課程管理區</strong>
  </div>
  @if ($success = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>上傳成功{{Session::get('name')}}</strong>
    </div>
  @endif
  @if ($success = Session::get('d_info'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>刪除成功</strong>
    </div>
  @endif
  @if (session('alert'))
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('alert') }}
    </div>
@endif

  <p>
  <button type="button" class="btn btn-default btn-sm" onclick="window.close();">
    <span class="glyphicon glyphicon-remove-circle"></span> 關閉分頁
  </button>
  <a href="{{ url('/authrize/classmanage/export/excel') }}/{{ $class_id }}/{{ $admin_id }}">
    <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-download"></span> 匯出修課名單</button>
  </a>
  </p>
  <div class="well form-horizontal">
    <fieldset>

      <!-- title -->
      <legend>
        <b>{{ $class->name }}</b>

      </legend>

      <!-- course table -->
        {{ csrf_field() }}
          <div class="table-responsive">
            @if($stu->isEmpty())
                <kbd>尚未上傳修課名單!&nbsp;</b></kbd>
            @else
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>學期別</th>
                  <th>課號</th>
                  <th>必/選修</th>
                  <th>學生學號</th>
                  <th>是否通過</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($stu as $stus)
                  <tr>
                    <td>{{$class->term}}</td>
                    <td>{{ $class->number }}</td>
                    <td>{{ $class->obligatory }}</td>
                    <td>{{ $stus->acct }}</td>
                    <td>@if($stus->is_pass == '1' )
                          <span class="glyphicon glyphicon-ok-circle" style="color:#00cc99;font-size:20px"></span>&nbsp;是
                        @else
                          <span class="glyphicon glyphicon-remove-circle" style="color:#ff1a1a;font-size:20px"></span>&nbsp;否
                        @endif
                    </td>
                    <td></td>
                    <td></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </fieldset>
      </div>
  </div>
</div>




@endsection
@section('js')

@endsection
