@extends('layouts.app')

@section('title', '查詢結果')

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10"><br>
  <button type="button" class="btn btn-default btn-sm" onclick="window.close();">
    <span class="glyphicon glyphicon-remove-circle"></span> 關閉分頁
  </button><br><br>
  <div class="well form-horizontal">
    <fieldset>

      <!-- title -->
      <legend><b>查詢結果</b></legend>
      @if($nothing == 1)
        <h4><b>查無結果!</b></h4>
      @else
        <!-- course table -->
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>學期別</th>
                <th>課程屬性</th>
                <th>課程名稱</th>
              </tr>
            </thead>
            <tbody>
            
              @foreach ($module_classes as $module_class)
              <tr class="info">
                <td>{{$module_class->term}}</td>
                <td>套裝課程</td>
                <td>{{$module_class->name}}</td>
                <!--班級管理-->
                <td>
                  <a href="{{ url('/authrize/classmanage/套裝課程') }}/{{$module_class->id}}/{{$admin_id}}" target="_blank">
                    <button type="button" class="btn btn-default btn-sm">
                      <span class="glyphicon glyphicon-edit"></span> 查看學生修課狀況
                    </button>
                  </a>
                </td>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      @endif
    </fieldset>
  </div>
</div>


@endsection

@section('css')
<style>
#new_class {
  /*float: right;*/
  /*background-color:#33cccc;*/
  /*border-color: #33cccc;*/
  /*transition-duration: 0.4s;*/
}
#new_class:hover {
  /*float: right;*/
  /*color:#ffffff;*/
  /*background-color:#248f8f;*/
  /*border-color: #248f8f;*/
}
#logout {
  /*float: right;*/
  background-color:#33cccc;
  border-color: #33cccc;
  transition-duration: 0.4s;
}
#logout:hover {
  /*float: right;*/
  color:#ffffff;
  background-color:#248f8f;
  border-color: #248f8f;
}
#edit_pwd{
  /*float: right;*/
  background-color: #808080;
  border-color: #808080;
  color: #ffffff;
}
#edit_pwd:hover{
  /*float: right;*/
  background-color:#737373;
  border-color: #595959;
  color: #ffffff;
}
#act{
  float:right;
}
*{
  font-family: Microsoft JhengHei;
}
</style>
<style>
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }
    </style>
@endsection

@section('js')
<script>

</script>
@endsection
