@extends('layouts.app')

@section('title', '最新公告管理')

@section('content')

<div class=""><br>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <br><h2 style="border-left:solid 2px #e6b3b3">&nbsp;新增-最新公告圖片</h2><br>
       <form  enctype="multipart/form-data" action="{{ asset('authrize/menu/photo') }}" method="post">
         {{ csrf_field() }}
         <div id="filetype">
           <h4>大標: </h4><input type="text" name="title" class="form-control"><br><br>
           <h4>小標: </h4><input type="text" name="subtitle" class="form-control"><br><br>
           <h4>字體顏色: <input  value="FFFFFF" name="color"class="jscolor form-control"><br><br>
           <h4>檔案:</h4>
           <div class="fileupload fileupload-new" data-provides="fileupload">
							<span class="btn btn-primary btn-file"><span class="fileupload-new"></span>
								<input type="file" name="photo"/></span>
							<span class="fileupload-preview"></span>
						</div> <br>
           <div class="row">
             <div class="col-sm-6">
             </div>
             <div class="col-sm-6">
               {{ csrf_field() }}
               <button type="submit" class="btn btn-success">確認</button>
             </div>
            </div>
         </div>

       </form>



    </div>
    <script src="/js/news/jscolor.js"></script>
    <script type="text/javascript">
      $('#color').colorpicker({});
      $(document).ready(function(){
          $("#show_filetype").click(function(){
              $("#articletype").hide(700);
              $("#filetype").show(700);
          });
      });

      $(document).ready(function(){
          $("#show_articletype").click(function(){
              $('#articletype').css('display','').show(700);

              $("#filetype").hide(700);
          });
      });

    </script>

@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('js')
@endsection
